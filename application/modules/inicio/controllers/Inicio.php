<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends MX_Controller {
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		// $this->load->view('principal');
		$this->load->view('formulario');
	}

	public function registrar()
	{
		// print_r($_POST);
		$data = $_POST;
		$this->load->view('datos', $data);
	}
}
